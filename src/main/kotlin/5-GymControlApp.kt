import java.util.*

open class GymControlReader(
    val usersIn:MutableList<String> = mutableListOf()
){
    open fun nextId() = ""
    open fun checkId(id:String){
        if (id in this.usersIn){
            println("$id Sortida")
            usersIn.remove(id)
        }else{
            println("$id Entrada")
            usersIn.add(id)
        }
    }
}

class GymControlManualReader(private val scanner: Scanner = Scanner(System.`in`)) : GymControlReader() {
    override fun nextId(): String = scanner.next()
}

/*
Use args. Here's the test input:
1548 8768 4874 1548 1354 1548 3586 1354

Expected output:
1548 Entrada
8768 Entrada
4874 Entrada
1548 Sortida
1354 Entrada
1548 Entrada
3586 Entrada
1354 Sortida
 */
fun main(args: Array<String>){
    //Automatic
    val controller = GymControlReader()
    for (id in args){
        controller.checkId(id)
    }
    //Manual (Works until 10 different IDs are entered.)
    val manualController = GymControlManualReader()
    do {
        println("Id:")
        val id = readln()
        manualController.checkId(id)
    }while (manualController.usersIn.size < 10)
}
