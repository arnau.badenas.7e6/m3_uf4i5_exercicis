class ArcSantMarti(
    private val colors:Array<String>
){
    fun isInRainbow(color:String):Boolean{
        return color in this.colors
    }
}

fun main(){
    val rainbow = ArcSantMarti(arrayOf("red", "orange", "yellow", "green", "blue", "indigo", "violet"))

    println("Write a color")
    val color = readln()
    print(rainbow.isInRainbow(color))
}