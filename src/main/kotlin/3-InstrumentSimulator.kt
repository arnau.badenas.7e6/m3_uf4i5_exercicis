open class Instrument()
{
    open fun makeSounds(times:Int){
        println("sound of silence")
    }
}

class Triangle(
    val resonancia:Int
) : Instrument() {
    override fun makeSounds(times:Int){
        for (i in 1..times){
            print("T")
            for (resonar in 1..this.resonancia){
                print("I")
            }
            print("NC ")
        }
        println()
    }
}

class Drump(
    val type:String
) : Instrument() {
    override fun makeSounds(times:Int){
        for (i in 1..times){
            print("T")
            for (resonar in 1..3){
                print(type)
            }
            print("M ")
        }
        println()
    }
}

private fun play(instruments: List<Instrument>) {
    for (instrument in instruments) {
        instrument.makeSounds(2) // plays 2 times the sound
    }
}

fun main() {
    val instruments: List<Instrument> = listOf(
        Triangle(5),
        Drump("A"),
        Drump("O"),
        Triangle(1),
        Triangle(5)
    )
    play(instruments)
}

