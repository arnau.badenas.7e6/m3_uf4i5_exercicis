open class Question(
    var id: Int,
    var isSolved: Boolean,
    var statement: String,
    open val solution: Any //If i use this i can change type in inherited classes
) {
    open fun checkSolution() {
        println("Introduce your answer:")
        val answer = readln()
        if (answer == solution) {
            isSolved = true
            println("Question solved!")
        } else {
            isSolved = false
            println("That's not the correct answer...")
        }
    }
}

class FreeTextQuestion(
    id: Int,
    isSolved: Boolean,
    statement: String,
    solution: String
) : Question(id, isSolved, statement, solution) {

}

class MultipleChoiceQuestion(
    id: Int,
    isSolved: Boolean,
    statement: String,
    solution: String,
    val possibleSolutions:Array<String>
) : Question(id, isSolved, statement, solution) {

    override fun checkSolution() {
        println("Possible solutions:")
        for (i in possibleSolutions){
            println(i)
        }
        do {
            println("Introduce your choice:")
            val answer = readln()
            if (answer == solution){
                isSolved = true
                println("Question solved!")
            }
            else{
                println("Sorry, that was not the answer.")
            }
            if (answer !in possibleSolutions){
                println("The answer you provided wasn't in the possible solutions!")
            }
        }while (answer !in possibleSolutions)

    }
}

class Quiz(
    val questionList: List<Question>
) {
    fun answerQuestions() {
        for (question in this.questionList) {
            println(
                """
                ----- Question ${question.id} -----
                ${question.statement}
            """.trimIndent()
            )
            question.checkSolution()
        }
    }

    fun printResults() {
        for (question in this.questionList) {
            println(
                """
                -- Question ${question.id} - Is it solved? ${question.isSolved}
            """.trimIndent()
            )
        }
    }
}

fun main() {
    val question1 = FreeTextQuestion(
        id = 1,
        isSolved = false,
        statement = "What is the capital of France?",
        solution = "Paris"
    )

    val question2 = MultipleChoiceQuestion(
        id = 2,
        isSolved = false,
        statement = "What is the largest country in the world?",
        possibleSolutions = arrayOf("Russia", "China", "USA", "Brazil"),
        solution = "Russia",
    )

    val question3 = FreeTextQuestion(
        id = 3,
        isSolved = false,
        statement = "What is the square root of 16?",
        solution = "4",
    )

    val question4 = MultipleChoiceQuestion(
        id = 4,
        isSolved = false,
        statement = "Who is the founder of Microsoft?",
        possibleSolutions = arrayOf("Bill Gates", "Steve Jobs", "Mark Zuckerberg", "Elon Musk"),
        solution = "Bill Gates"
    )

    val question5 = FreeTextQuestion(
        id = 5,
        isSolved = false,
        statement = "What is the formula for water?",
        solution = "H2O",
    )

    val question6 = MultipleChoiceQuestion(
        id = 6,
        isSolved = false,
        statement = "Which planet is closest to the sun?",
        possibleSolutions = arrayOf("Mercury", "Venus", "Mars", "Jupiter"),
        solution = "Mercury"
    )

    val question7 = FreeTextQuestion(
        id = 7,
        isSolved = false,
        statement = "What is the tallest mountain in the world?",
        solution = "Mount Everest",
    )

    val question8 = MultipleChoiceQuestion(
        id = 8,
        isSolved = false,
        statement = "What is the currency of Japan?",
        possibleSolutions = arrayOf("Yen", "Dollar", "Euro", "Pound Sterling"),
        solution = "Yen"
    )

    val question9 = FreeTextQuestion(
        id = 9,
        isSolved = false,
        statement = "What is the largest animal in the world?",
        solution = "Blue whale",
    )

    val question10 = MultipleChoiceQuestion(
        id = 10,
        isSolved = false,
        statement = "What is the chemical symbol for gold?",
        possibleSolutions = arrayOf("Au", "Ag", "Cu", "Fe"),
        solution = "Au"
    )

    val quiz = Quiz(
        listOf(
            question1,
            question2,
            question3,
            question4,
            question5,
            question6,
            question7,
            question8,
            question9,
            question10,
        )
    )

    quiz.answerQuestions()
    quiz.printResults()
}