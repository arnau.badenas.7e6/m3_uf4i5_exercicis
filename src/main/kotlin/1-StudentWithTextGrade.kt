class Student(
    private val nom:String,
    private val nota:String
)
{
    override fun toString(): String {
        return "Nom: ${this.nom}, Nota: ${this.nota}"
    }
}

fun main(){
    val estudiant1 = Student("Arnau","FAILED")
    val estudiant2 = Student("Paula","EXCELLENT")
    val estudiants = arrayOf(estudiant1,estudiant2)
    estudiants.forEach { println(it.toString()) }
}